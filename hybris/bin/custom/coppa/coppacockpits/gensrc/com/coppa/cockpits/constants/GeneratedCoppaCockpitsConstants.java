/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 2 Apr, 2019 9:05:23 AM                      ---
 * ----------------------------------------------------------------
 */
package com.coppa.cockpits.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedCoppaCockpitsConstants
{
	public static final String EXTENSIONNAME = "coppacockpits";
	
	protected GeneratedCoppaCockpitsConstants()
	{
		// private constructor
	}
	
	
}
